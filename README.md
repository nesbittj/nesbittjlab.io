
This is the source code for [nesbittj.gitlab.io](https://nesbittj.gitlab.io/).

It is a static site build with [jekyll](https://jekyllrb.com/). It is hosted on [gitlab.com](https://gitlab.com/) using [gitlab pages](https://about.gitlab.com/features/pages/). The source code is available at [gitlab.com/nesbittj](https://gitlab.com/nesbittj/nesbittj.gitlab.io).

The site uses a custom theme. I found inspiration from [github.com/mojombo](https://github.com/mojombo/mojombo.github.io), [GOV.UK elements](https://govuk-elements.herokuapp.com/) and [Material Design](https://material.io/).

Please see the [LICENSE file](https://gitlab.com/nesbittj/nesbittj.gitlab.io/blob/master/LICENSE) for licensing information.
