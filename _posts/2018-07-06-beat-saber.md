---
title: "Beat Saber"
layout: post
date:   2018-07-06 22:00:00 +0100
permalink: /post/beat-saber/
excerpt: "Virtual reality rhythm game"
---

> Purchased on Steam for GBP 15.49. <span>[Steam store page](https://store.steampowered.com/app/620980/Beat_Saber/)
>
> Release date: Early Access 1 May, 2018. [www.beatsaber.com](http://www.beatsaber.com/)
>
> Developer: Beat Games / [Hyperbolic Magnetism](http://www.hyperbolicmagnetism.com/)

*Beat Saber* is a rhythm game built for VR. At the time of writing the game supports the HTC Vive, Oculus Rift and Windows Mixed Reality, with PS VR in development. I played using the HTC Vive.

*Beat Saber* challenges the player to slash blocks as they race towards you to the beat of the music. The weapon of choice is two colour coded lightsabers, one in each hand.

Each block has an arrow indicating the direction it must be hit. They are also colour coded, red and blue, specifying a left or right handed strike.

![Gameplay screenshot](/img/post/beat-saber/6365 shot_1920_1080.jpg)

Blocks move on tracks towards you. Some will travel straight, others will turn upside down, or turn sideways, keeping the player on their toes. This creates fast patterns of movement like percussion to match the rhythm of the music. Most tracks also include moving walls which must be avoided by moving ones head out of their path; these can demand full body movement, but also provide vital respite from frantic stretches. Many repeated sessions will be needed to get that perfect run.

The game is best played at room-scale VR with plenty of space to move your arms. But also supports sitting and standing, as well as a single handed option.

Increasing the difficulty adds more blocks closer together and can included special blocks like bombs to be avoided. I am an inexperienced rhythm game player and found the hard difficulty (3/4) an enjoyable challenge.

![Gameplay screenshot](/img/post/beat-saber/2575 shot_1920_1080.jpg)

The early access title launched with an [original score](https://fanlink.to/BeatSaber) by Jaroslav Beck, who's name is attached to well known Blizzard and EA titles. The number of tracks is limited to just 10, but they do provide a good selection and the tracks are are well crafted for purpose with a few that really stand out. You will find yourself dancing along to the music as you time your lightsaber strikes. The game is still in early access and news updates from the developers give assurance of more original music to come.

There is a promised level editor in the works enabling community created content. Also worth mentioning is the large modding community, one such project boasting a few thousand tracks with hundreds of thousands of downloads.

*Beat Saber* is a huge amount of fun and even in this pre-release state is more polished and well thought out than many other VR titles. It is a VR game I look forward to returning to many times. It looks and sounds great, lightsaber strikes slice with accuracy and solid feedback. Well worth picking up for such a reasonable price.