---
layout: page
title: About
permalink: /about/
---

Welcome to my blog. In it I will be talking mostly about computer games. The site is in it's early stages, it doesn't even have a title yet. It is open source, see below for more.

A bit about how this site is put together:

This is a static site build with [jekyll](https://jekyllrb.com/). It is hosted on [gitlab.com](https://gitlab.com/) using [gitlab pages](https://about.gitlab.com/features/pages/). The source code is available at [gitlab.com/nesbittj](https://gitlab.com/nesbittj/nesbittj.gitlab.io).

The site uses a custom theme. I found inspiration from [github.com/mojombo](https://github.com/mojombo/mojombo.github.io), [GOV.UK elements](https://govuk-elements.herokuapp.com/) and [Material Design](https://material.io/).

Please see the [LICENSE file](https://gitlab.com/nesbittj/nesbittj.gitlab.io/blob/master/LICENSE) for more information.
