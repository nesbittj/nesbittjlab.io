---
layout: default
---

<div id="home">
    <h1>Posts</h1>
    <ul class="posts">
        {% for post in site.posts %}
        {% capture day %}{{ post.date | date: '%m%Y' }}{% endcapture %}
        {% capture nday %}{{ post.next.date | date: '%m%Y' }}{% endcapture %}
        <li>
            {% if day != nday %}
                <span>{{ post.date | date: '%B %Y' }}</span><br>
            {% endif %}
            <a href="{{ post.url }}">{{ post.title }}</a>
        </li>
        {% endfor %}
    </ul>
</div>